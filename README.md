# 恐龙云 TV

### 介绍
恐龙云 TV ：能看电视（目前不可以）

### 软件架构
WPF，支持 32 位。
目前仅支持安装了新版 Microsoft Edge 的 Windows 机器运行。


### 许可协议

我们技术能力有限，不会搞病毒，请您放心使用。您阅读完毕并安装即视为同意此协议，最终解释权归我们所有。

### 使用说明

无

### 参与贡献

#### 下载 Beta 版尝鲜

[查看所有发行版](https://gitee.com/pink-cat/Newgen_TV/releases)
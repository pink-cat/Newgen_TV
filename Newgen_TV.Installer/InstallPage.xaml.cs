﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wsh = IWshRuntimeLibrary;

namespace Newgen_TV.Installer
{
    /// <summary>
    /// InstallPage.xaml 的交互逻辑
    /// </summary>
    public partial class InstallPage : UserControl
    {
        public InstallPage()
        {
            InitializeComponent();
            targetTB.Text = @"C:\Program Files\Newgen_TV";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog o = new System.Windows.Forms.FolderBrowserDialog();
            o.ShowDialog();
            targetTB.Text = o.SelectedPath.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string Target = targetTB.Text;
            Directory.CreateDirectory(Target);
            string zipfile = Target + @"\FILE";
            string targetfolder = Target;
            Directory.CreateDirectory(targetfolder);

            FileStream fs = new FileStream(zipfile, FileMode.Create);
            BinaryWriter binw = new BinaryWriter(fs);
            binw.Write(Resource1.main);
            binw.Close();
            fs.Close();

            System.IO.Compression.ZipFile.ExtractToDirectory(zipfile, targetfolder);
            var JunkFile = new FileInfo(zipfile);
            JunkFile.Delete();

            wsh.WshShell shell = new wsh.WshShell();
            string lnkPath = Target+ @"\恐龙云 TV For Windows.exe";

            wsh.IWshShortcut lnk = shell.CreateShortcut
                (@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\" + "恐龙云TV" + ".lnk");
            lnk.TargetPath = lnkPath;
            lnk.Description = "欢迎看电视";//ToolTip
            lnk.IconLocation = lnkPath;//icon
            lnk.Save();

            MessageBox.Show("安装完成，但没有完全完成，正在退出");
            Process.Start(Target);
            App.Current.MainWindow.Close();
        }
    }
}

﻿using Newgen_电视;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Newgen_TV
{
    /// <summary>
    /// about.xaml 的交互逻辑
    /// </summary>
    public partial class about : Window
    {
        public about()
        {
            InitializeComponent();
        }

        private void close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void checkNew(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("暂未开发","自动更新");
        }

        private void Fadian(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://afdian.net/@GBCLstudio");
        }

        private void thanks(object sender, RoutedEventArgs e)
        {
            thanks win = new Newgen_TV.thanks();
            win.Show();
        }

        private void SupportSoftPink(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://afdian.net/@SoftPinkSP");
        }

        private void whatIsNew(object sender, RoutedEventArgs e)
        {
            whatisnew win = new Newgen_TV.whatisnew();
            win.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Newgen_TV
{
    /// <summary>
    /// thanks.xaml 的交互逻辑
    /// </summary>
    public partial class thanks : Window
    {
        public thanks()
        {
            InitializeComponent();
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void KnowMore(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitee.com/fanbal/fantom");
        }
    }
}

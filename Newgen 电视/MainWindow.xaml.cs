﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using Microsoft.Web.WebView2.Core;
using HandyControl.Tools.Extension;
using System.Windows.Navigation;
using System.Windows.Input;
using Newgen_电视;

namespace Newgen_TV
{

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    /// 打开新窗口
    class DialogHelper
    {
        //从Handle中获取Window对象
        static Window GetWindowFromHwnd(IntPtr hwnd)
        {
            return (Window)HwndSource.FromHwnd(hwnd).RootVisual;
        }

        //GetForegroundWindow API
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        //调用GetForegroundWindow然后调用GetWindowFromHwnd
        static Window GetTopWindow()
        {
            var hwnd = GetForegroundWindow();
            if (hwnd == null)
                return null;

            return GetWindowFromHwnd(hwnd);
        }

        //显示对话框并自动设置Owner，可以理解为子窗体添加父窗体
        public static void ShowDialog(Window win)
        {
            //win.Owner = GetTopWindow();
            win.ShowInTaskbar = true;       //false表示不显示新的窗口，默认当前打开窗口为一个子窗口（不会显示两个窗口）
            win.ShowDialog();
        }
    }
    public partial class MainWindow : Window
    {
        public object webview { get; private set; }
        public UserControl1 UserContent { get; private set; }

        public MainWindow()
        {
            this.Topmost = false;
            this.WindowStyle = System.Windows.WindowStyle.None;
            this.WindowState = System.Windows.WindowState.Maximized;
            InitializeComponent();
            //DialogHelper.ShowDialog(new MainWindow());
        }

        private void open_New(object sender, RoutedEventArgs e)
        {
            //新建窗口
            DialogHelper.ShowDialog(new MainWindow());
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            //关闭当前窗口
            this.Close();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void open_About(object sender, RoutedEventArgs e)
        {
            //打开关于页面
            about win = new about();
            win.Show();
        }

        private void open_N1(object sender, RoutedEventArgs e)
        {
            BD.Child = new UserControl1();
        }
        private void open_Home(object sender, RoutedEventArgs e)
        {
            //返回主页
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void open_No1(object sender, RoutedEventArgs e)
        {
            //测试 Webview2
            No1 no1 = new No1();
            no1.Show();
        }
        private void Grid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)//Esc键
            {
                this.WindowState = System.Windows.WindowState.Normal;
                this.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BD.Child = new UserControl2();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            BD.Child = new UserControl3();
        }

        private void open_Advice(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://forms.office.com/r/E6bwYBn1hC");
        }

        private void open_Mianze(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://tv.gcxstudio.cn/mzsm.html");
        }

        private void open_Xuke(object sender, RoutedEventArgs e)
        {
            //打开许可协议
            xuke win = new xuke();
            win.Show();
        }

        private void open_Shuaiguo(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://live.bilibili.com/14995647");
        }
    }

}

    
    
 
